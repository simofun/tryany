﻿namespace Simofun.Unity.Data.Configuration.Demo
{
	using Newtonsoft.Json;
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using UnityEngine;

	[CreateAssetMenu(
		fileName = nameof(PersistentData),
		menuName = "ScriptableObjectPersistent/" + nameof(PersistentData))]
	public class PersistentData : SimCustomAssetConfigBase
	{
		#region Unity Fields
		[SerializeField, JsonProperty]
		int value;
		#endregion

		#region Properties
		[JsonIgnore]
		public int Value { get => this.value; set => this.value = value; }
		#endregion
	}
}
