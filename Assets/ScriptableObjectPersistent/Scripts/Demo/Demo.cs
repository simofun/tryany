﻿using TMPro;

namespace Simofun.Unity.Data.Configuration.Demo
{
	using UnityEngine;

	public class Demo : MonoBehaviour
	{
		#region Unity Fields
		[SerializeField]
		PersistentData data;

		[SerializeField]
		TextMeshProUGUI text;
		#endregion

		#region Properties
		public PersistentData Data { get => this.data; set => this.data = value; }

		public TextMeshProUGUI Text { get => this.text; set => this.text = value; }
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		protected virtual void Start() => this.Text.text = this.Data.Value.ToString();

		/// <inheritdoc />
		protected virtual void Update()
		{
			if (!Input.GetKeyDown(KeyCode.Space))
			{
				return;
			}

			this.Data.Value++;
			this.Text.text = this.Data.Value.ToString();
		}
		#endregion
	}
}
