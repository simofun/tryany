﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigServiceMenuItems.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguç</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Editor.Data.Configuration.CustomAsset
{
	using Simofun.Unity.Data.Configuration.CustomAsset;
#if UNITY_EDITOR
	using UnityEditor;

	public static class SimCustomAssetConfigServiceMenuItems
	{
		#region Fields
		const string seperator = "/";
		#endregion

		#region Methods
		[MenuItem(nameof(Simofun) + seperator + nameof(Unity) + seperator + nameof(Data) + seperator
			+ nameof(Configuration) + seperator + "Delete Save")]
		public static void DeleteSave() => SimCustomAssetConfigService.Instance.DeleteSave();

		[MenuItem(nameof(Simofun) + seperator + nameof(Unity) + seperator + nameof(Data) + seperator
			+ nameof(Configuration) + seperator + "Reset Configs")]
		public static void ResetConfigs() => SimCustomAssetConfigService.Instance.Reset();

		[MenuItem(nameof(Simofun) + seperator + nameof(Unity) + seperator + nameof(Data) + seperator
			+ nameof(Configuration) + seperator + "Hard Reset")]
		public static void HardReset() => SimCustomAssetConfigService.Instance.HardReset();
		#endregion
	}
#endif
}
