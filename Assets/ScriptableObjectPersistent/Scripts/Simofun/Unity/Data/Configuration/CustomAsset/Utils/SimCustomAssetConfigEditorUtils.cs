//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigEditorUtils.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Hasan Emre Tongu�</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.Utils
{
    using Simofun.Unity.Data.Configuration.CustomAsset.Model;
#if UNITY_EDITOR
    using System.Collections.Generic;
    using System.Linq;
    using UnityEditor;
    using UnityEngine;

    public static class SimCustomAssetConfigEditorUtils
    {
        #region Properties
        public static SimCustomAssetConfigServiceConfigBase Config
        {
            get
            {
                var configGuids = AssetDatabase.FindAssets($"t:{nameof(SimCustomAssetConfigServiceConfig)}");
                if (configGuids == null || configGuids.Length == 0)
                {
                    return null;
                }

                return AssetDatabase.LoadAssetAtPath<SimCustomAssetConfigServiceConfig>(
                    AssetDatabase.GUIDToAssetPath(configGuids.FirstOrDefault()));
            }
        }
        #endregion

        #region Public Methods
        public static bool IsDefaultsExist(SimCustomAssetConfigBase asset)
        {
            var isExist = GetDefault(asset) != null;
            if (!isExist)
            {
                CreateDefaults(asset);
            }

            return isExist;
        }

        public static void CheckDefaults(SimCustomAssetConfigBase asset) => IsDefaultsExist(asset);

        public static void CreateDefaults(SimCustomAssetConfigBase asset)
        {
            if (EditorUtility.IsDirty(asset))
            {
                return;
            }

            var defaultInstance = ScriptableObject.CreateInstance(asset.GetType());
            EditorUtility.CopySerialized(asset, defaultInstance);
            defaultInstance.name = "Default";

            AssetDatabase.AddObjectToAsset(defaultInstance, asset);
            AssetDatabase.SaveAssets();

            EditorUtility.SetDirty(asset);
            EditorUtility.SetDirty(defaultInstance);
        }

        public static void LoadDefaults(SimCustomAssetConfigBase asset)
        {
            Copy(asset, GetDefault(asset));
        }

        public static void UpdateDefaults(SimCustomAssetConfigBase asset)
        {
            Copy(GetDefault(asset), asset);
        }

        public static void Copy(Object source, Object target)
        {
            if (source == null || target == null)
            {
                return;
            }

            var assetName = source.name;
            EditorUtility.CopySerialized(target, source);
            source.name = assetName;
        }

        public static Object GetDefault(Object source)
        {
            var path = AssetDatabase.GetAssetPath(source);
            var defaultAsset = AssetDatabase.LoadAllAssetsAtPath(path)
                .FirstOrDefault(asset => asset.name.Equals("Default"));

            return defaultAsset;
        }

        public static T LoadFromPath<T>(string path) where T : ScriptableObject =>
            AssetDatabase.LoadAssetAtPath<T>(path);

        public static T[] LoadAll<T>() where T : ScriptableObject =>
            LoadFromGuids<T>(AssetDatabase.FindAssets(string.Format("t:{0}", nameof(ScriptableObject))));

        public static T[] LoadFromGuids<T>(string[] guids) where T : ScriptableObject
        {
            var objectList = new List<T>();
            foreach (var guid in guids)
            {
                var obj = LoadFromPath<T>(AssetDatabase.GUIDToAssetPath(guid));
                if (obj == null)
                {
                    continue;
                }

                objectList.Add(obj);
            }

            return objectList.ToArray();
        }

        public static T[] LoadSelected<T>() where T : ScriptableObject => LoadFromGuids<T>(Selection.assetGUIDs);
        #endregion
    }
#endif
}
