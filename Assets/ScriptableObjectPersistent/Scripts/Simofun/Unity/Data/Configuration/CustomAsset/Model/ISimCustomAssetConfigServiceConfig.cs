﻿//-----------------------------------------------------------------------
// <copyright file="ISimCustomAssetConfigServiceConfig.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguç</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.Model
{
	using Simofun.IO;
	using Simofun.Unity.Data.Configuration.Model;
	using Simofun.Unity.Data.Model;
	using UnityEngine;

	public interface ISimCustomAssetConfigServiceConfig : ISimRootPath
	{
		#region Properties
		public bool IsBinary { get; }

		public SimAppDataPath DataPath { get; }

		public SimJsonSerializerType JsonSerializerType { get; }

		public string Directory { get; }

		public string FileExtension { get; }
		#endregion

		#region Public Methods
		string GetFilePath(Object obj, params string[] addons);
		#endregion
	}
}
