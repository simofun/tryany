namespace Simofun.Unity.Data.Configuration.CustomAsset
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.Data.Configuration.CustomAsset.Utils;
#if UNITY_EDITOR
	using System.Collections.Generic;
	using System.Linq;
	using UnityEditor;

	public static class SimConfigValidateProcessor
	{
		#region Static Fields
		readonly static List<SimConfigValidationTracker> configList =
			new List<SimConfigValidationTracker>();
		#endregion

		#region Unity Methods
		static SimConfigValidateProcessor()
		{
			RegisterEventHandlers();
		}
		#endregion

		#region Methods
		#region Event Handlers
		#region Registration
		static void RegisterEventHandlers()
		{
			Selection.selectionChanged -= HandleOnSelectionChange;
			Selection.selectionChanged += HandleOnSelectionChange;
		}
		#endregion

		static void HandleOnSelectionChange()
		{
			ValidateSelected();
		}
		#endregion
		#endregion

		#region Public Static Methods
		static void ValidateSelected()
		{
			var selectedConfigs = SimCustomAssetConfigEditorUtils.LoadSelected<SimCustomAssetConfigBase>();
			Clear();

			foreach (var config in selectedConfigs)
			{
				Validate(config);
			}
		}

		static void Clear()
		{
			foreach (var config in configList)
			{
				config.Dispose();
			}

			configList.Clear();
		}

		static void TrackAll()
		{
			var allConfigs = SimCustomAssetConfigEditorUtils.LoadAll<SimCustomAssetConfigBase>();
			foreach (var config in allConfigs)
			{
				Stop(config);
				Validate(config);
			}
		}

		public static void Validate(SimCustomAssetConfigBase config)
		{
			if (configList.Any(tracker => tracker.Config == config))
			{
				return;
			}

			var tracker = new SimConfigValidationTracker(config);
			configList.Add(tracker);
		}

		public static void Stop(SimCustomAssetConfigBase config)
		{
			var tracker = configList.FirstOrDefault(tracker => tracker.Config == config);
			if (tracker == null)
			{
				return;
			}

			tracker.Dispose();
			configList.Remove(tracker);
		}
		#endregion
	}
#endif
}
