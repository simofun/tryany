﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigServiceBase`1.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguç</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset
{
	public abstract class SimCustomAssetConfigServiceBase<T>
		: SimCustomAssetConfigServiceBase<T, T> where T : SimCustomAssetConfigServiceBase<T>, new()
	{
		#region Protected Constructors
		protected SimCustomAssetConfigServiceBase() : base()
		{
		}
		#endregion
	}
}
