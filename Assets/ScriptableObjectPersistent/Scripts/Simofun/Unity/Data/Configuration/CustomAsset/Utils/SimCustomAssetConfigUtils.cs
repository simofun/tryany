//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigUtils.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Hasan Emre Tongu�</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.Utils
{
	using Newtonsoft.Json;
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.Data.Configuration.Model;
	using System.IO;
	using System.Runtime.Serialization.Formatters.Binary;
	using UnityEngine;

	public static class SimCustomAssetConfigUtils
	{
		#region Public Methods
		public static string Read(
			ISimCustomAssetConfigServiceConfig config,
			BinaryFormatter binaryFormatter,
			string path)
		{
			if (!config.IsBinary)
			{
				return File.ReadAllText(path);
			}

			var file = File.Open(path, FileMode.Open);
			var res = binaryFormatter.Deserialize(file) as string;
			file.Close();

			return res;
		}

		public static void PopulateObject(
			ISimCustomAssetConfigServiceConfig config,
			string json,
			object objectToOverwrite)
		{
			switch (config.JsonSerializerType)
			{
				case SimJsonSerializerType.Unity:
					{
						JsonUtility.FromJsonOverwrite(json, objectToOverwrite);

						break;
					}
				case SimJsonSerializerType.JsonNet:
					{
						JsonConvert.PopulateObject(json, objectToOverwrite);

						break;
					}
				default:
					{
						break;
					}
			}
		}

		public static void Write(
			ISimCustomAssetConfigServiceConfig config,
			BinaryFormatter binaryFormatter,
			string path,
			object obj)
		{
			var serialized = ReadJson(config, obj);
			if (!config.IsBinary)
			{
				var file = new FileInfo(path);
				if (!file.Directory.Exists)
				{
					file.Directory.Create();
				}

				File.WriteAllText(path, serialized);
#if UNITY_EDITOR
				UnityEditor.AssetDatabase.Refresh();
#endif
			}
			else
			{
				var file = File.Create(path);
				binaryFormatter.Serialize(file, serialized);
				file.Close();
			}
		}
		#endregion

		#region Methods
		static string ReadJson(ISimCustomAssetConfigServiceConfig config, object obj)
		{
			switch (config.JsonSerializerType)
			{
				case SimJsonSerializerType.Unity:
					{
						return JsonUtility.ToJson(obj);
					}
				case SimJsonSerializerType.JsonNet:
					{
						return JsonConvert.SerializeObject(obj);
					}
				default:
					{
						return null;
					}
			}
		}
		#endregion
	}
}
