﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigServiceConfigBase.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguç</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.Model
{
	using Simofun.Unity.Data.Configuration.Model;
	using Simofun.Unity.Data.Model;
	using UnityEngine;

	public abstract class SimCustomAssetConfigServiceConfigBase : ScriptableObject, ISimCustomAssetConfigServiceConfig
	{
		#region Properties
		public abstract bool IsBinary { get; }

		public abstract SimAppDataPath DataPath { get; }

		public abstract SimJsonSerializerType JsonSerializerType { get; }

		public abstract string Directory { get; }

		public abstract string FileExtension { get; }

		public abstract string RootPath { get; set; }
		#endregion

		#region Public Methods
		public abstract string GetFilePath(Object obj, params string[] addons);
		#endregion
	}
}
