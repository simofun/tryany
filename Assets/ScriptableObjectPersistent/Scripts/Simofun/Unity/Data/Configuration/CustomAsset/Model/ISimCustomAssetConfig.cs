﻿//-----------------------------------------------------------------------
// <copyright file="ISimCustomAssetConfig.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguç</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.Model
{
	using Simofun.Unity.LifeCycle;

	public interface ISimCustomAssetConfig : ISimResettable
	{
		#region Events
		event System.Action<SimCustomAssetConfigBase> OnEditorValidation;
		#endregion
	}
}
