﻿//-----------------------------------------------------------------------
// <copyright file="ISimCustomAssetConfigService.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguç</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.LifeCycle;

	public interface ISimCustomAssetConfigService : ISimInitializable, ISimNamable, ISimResettable
	{
		#region Properties
		ISimCustomAssetConfigServiceConfig Config { get; }
		#endregion

		#region Methods
		void DeleteSave();

		void HardReset();

		void Load();

		void Save();
		#endregion
	}
}
