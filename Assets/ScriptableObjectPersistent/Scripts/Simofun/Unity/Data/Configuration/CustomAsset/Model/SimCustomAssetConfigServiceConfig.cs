﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigServiceConfig.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguç</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.Model
{
	using Simofun.Unity.Data.Configuration.Model;
	using Simofun.Unity.Data.Model;
	using Sirenix.OdinInspector;
	using System.IO;
	using UnityEngine;

	[CreateAssetMenu(
		fileName = nameof(SimCustomAssetConfigServiceConfig),
		menuName = nameof(Simofun) + "/" + nameof(Unity) + "/" + nameof(Data) + "/" + nameof(Configuration) + "/"
					 + nameof(Model) + "/" + nameof(SimCustomAssetConfigServiceConfig))]
	public class SimCustomAssetConfigServiceConfig : SimCustomAssetConfigServiceConfigBase,
		ISimCustomAssetConfigServiceConfig
	{
		#region Unity Fields
		[Title(nameof(SimCustomAssetConfigServiceConfig), "Settings")]
		[SerializeField]
		SimAppDataPath dataPath = SimAppDataPath.PersistentDataPath;

		[SerializeField]
		SimJsonSerializerType jsonSerializerType = SimJsonSerializerType.JsonNet;

		[SerializeField]
		bool isBinary = true;

		[SerializeField]
		string[] directoryNames = new string[] { "Data", "Configuration" };

		[SerializeField]
		string fileExtension = ".cache";
		#endregion

		#region Fields
		string directory;

		string rootPath;
		#endregion

		#region Properties
		/// <inheritdoc />
		public override bool IsBinary => this.isBinary;

		/// <inheritdoc />
		public override SimAppDataPath DataPath => this.dataPath;

		/// <inheritdoc />
		public override SimJsonSerializerType JsonSerializerType => this.jsonSerializerType;

		/// <inheritdoc />
		public override string Directory
		{
			get
			{
				if (!string.IsNullOrEmpty(this.directory))
				{
					return this.directory;
				}

				return this.directory = Path.Combine(this.directoryNames);
			}
		}

		/// <inheritdoc />
		public override string FileExtension => this.fileExtension;

		/// <inheritdoc />
		public override string RootPath
		{
			get
			{
				if (this.rootPath != null)
				{
					return this.rootPath;
				}

				switch (this.DataPath)
				{
					case SimAppDataPath.Default:
						{
							return Path.Combine(Application.dataPath, this.Directory);
						}
					case SimAppDataPath.PersistentDataPath:
						{
							return Path.Combine(Application.persistentDataPath, this.Directory);
						}
					case SimAppDataPath.Resources:
					default:
						{
							return null;
						}
				}
			}

			set { this.rootPath = value; }
		}
		#endregion

		#region Public Methods
		public override string GetFilePath(Object obj, params string[] addons) =>
			Path.Combine(
				this.RootPath,
				$"{obj.name}_{obj.GetInstanceID()}_{string.Join("_", addons)}{this.FileExtension}");
		#endregion
	}
}
