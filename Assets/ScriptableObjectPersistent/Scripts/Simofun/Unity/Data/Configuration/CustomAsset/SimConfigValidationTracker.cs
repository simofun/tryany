namespace Simofun.Unity.Data.Configuration.CustomAsset
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.Data.Configuration.CustomAsset.Utils;
#if UNITY_EDITOR

	public class SimConfigValidationTracker
	{
		#region Fields
		SimCustomAssetConfigBase config;
		#endregion

		#region Properties
		public SimCustomAssetConfigBase Config => this.config;
		#endregion

		#region Constructors
		public SimConfigValidationTracker(SimCustomAssetConfigBase config)
		{
			if (config == null)
			{
				return;
			}

			this.config = config;
			this.RegisterEventHandlers();
		}
		#endregion

		#region Public Methods
		public void Dispose() => this.UnRegisterEventHandlers();
		#endregion

		#region Methods
		#region Event Handlers
		#region Registration
		void RegisterEventHandlers()
		{
			if (this.Config == null)
			{
				return;
			}

			this.Config.OnEditorValidation += this.HandleOnValidation;
		}

		void UnRegisterEventHandlers()
		{
			if (this.Config == null)
			{
				return;
			}

			this.Config.OnEditorValidation -= this.HandleOnValidation;
		}
		#endregion

		void HandleOnValidation(SimCustomAssetConfigBase obj) => SimCustomAssetConfigEditorUtils.CheckDefaults(obj);
		#endregion
		#endregion
	}
#endif
}
