﻿//-----------------------------------------------------------------------
// <copyright file="SimScriptableConfigService`2.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguç</author>
//-----------------------------------------------------------------------

using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;

namespace Simofun.Unity.Data.Configuration.CustomAsset
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Model;
	using Simofun.Unity.Data.Configuration.CustomAsset.Utils;
	using Simofun.Unity.DesignPatterns.Singleton;
	using Sirenix.OdinInspector;
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Runtime.Serialization.Formatters.Binary;
	using UnityEngine;

	public abstract class SimCustomAssetConfigServiceBase<TInstance, TConcrete>
		: SimGlobalSingletonBase<TInstance, TConcrete>, ISimCustomAssetConfigService
		where TConcrete : SimCustomAssetConfigServiceBase<TInstance, TConcrete>, TInstance, new()
	{
		#region Unity Fields
		[Title(nameof(SimCustomAssetConfigService), "Settings")]
		[SerializeField, ValueDropdown(nameof(SelectConfigs))]
		SimCustomAssetConfigServiceConfigBase config;
		#endregion

		#region Fields
		readonly BinaryFormatter binaryFormatter = new BinaryFormatter();
		#endregion

		#region Properties
		/// <inheritdoc />
		public virtual ISimCustomAssetConfigServiceConfig Config
		{
			get
			{
				if (this.config != null)
				{
					return this.config;
				}

				this.config = Resources.Load<SimCustomAssetConfigServiceConfigBase>(
					"Data/Configuration/" + nameof(SimCustomAssetConfigServiceConfig));
				if (this.config == null)
				{
					Debug.LogError(
						$"Could not find any '{nameof(SimCustomAssetConfigServiceConfigBase)}' file under path which is specified by config.");

					return null;
				}

				return this.config;
			}

			set
			{
				this.config = value as SimCustomAssetConfigServiceConfigBase;
			}
		}
		#endregion

		#region Protected Properties
		protected virtual BinaryFormatter BinaryFormatter => this.binaryFormatter;
		#endregion

		#region Unity Methods
		/// <inheritdoc />
		public virtual void Reset() => this.Foreach((SimCustomAssetConfigBase config, string path) => config.Reset());

		/// <inheritdoc />
		protected virtual void OnDisable() => this.Save();
		#endregion

		#region Public Methods
		#region ISimInitializable Methods
		/// <inheritdoc />
		public override void Initialize()
		{
			base.Initialize();

			this.Load();

			this.RegisterEventHandlers();
		}
		#endregion

		/// <inheritdoc />
		public virtual void HardReset()
		{
			this.DeleteSave();
			this.Reset();
		}
		#endregion

		#region Public Methods
		public virtual void DeleteSave()
		{
			foreach (var config in this.GetConfigs())
			{
				File.Delete(this.Config.GetFilePath(config));
			}
		}

		public virtual void Load() => this.LoadInternal(this.GetConfigs());

		public virtual void Save() =>
			this.Foreach((SimCustomAssetConfigBase config, string path) =>
				SimCustomAssetConfigUtils.Write(this.Config, this.BinaryFormatter, path, config));
		#endregion

		#region Protected Methods
		#region Event Handlers
		#region Initialization
		protected virtual void RegisterEventHandlers() => UnitySceneManager.sceneLoaded += this.HandleOnSceneLoaded;

		protected virtual void UnRegisterEventHandlers() => UnitySceneManager.sceneLoaded -= this.HandleOnSceneLoaded;
		#endregion

		protected virtual void HandleOnSceneLoaded(
			UnityEngine.SceneManagement.Scene scene,
			UnityEngine.SceneManagement.LoadSceneMode mode) => this.LoadInternal(this.GetLoadedConfigs());
		#endregion

		protected virtual void LoadInternal(IEnumerable<SimCustomAssetConfigBase> configs) => this.Foreach(
			(SimCustomAssetConfigBase config, string path) =>
			{
				var fileExist = File.Exists(path);
				if (!fileExist)
				{
					return;
				}

				SimCustomAssetConfigUtils.PopulateObject(
					this.Config,
					SimCustomAssetConfigUtils.Read(this.Config, this.BinaryFormatter, path),
					config);
			});
		#endregion

		#region Methods
		System.Collections.IEnumerable SelectConfigs() =>
			Resources.LoadAll<SimCustomAssetConfigServiceConfigBase>(string.Empty);

		IEnumerable<SimCustomAssetConfigBase> GetLoadedConfigs() =>
			Resources.FindObjectsOfTypeAll<SimCustomAssetConfigBase>();

		IEnumerable<SimCustomAssetConfigBase> GetResourcesConfigs() =>
			Resources.LoadAll<SimCustomAssetConfigBase>(string.Empty);

		IEnumerable<SimCustomAssetConfigBase> GetConfigs() =>
			this.GetResourcesConfigs().Concat(this.GetLoadedConfigs()).Distinct();

		void Foreach(Action<SimCustomAssetConfigBase, string> onConfig) => this.Foreach(this.GetConfigs(), onConfig);

		void Foreach(IEnumerable<SimCustomAssetConfigBase> configs, Action<SimCustomAssetConfigBase, string> onConfig)
		{
			foreach (var config in configs)
			{
				if (config == null)
				{
					continue;
				}

				var filePath = this.Config.GetFilePath(config);
				onConfig?.Invoke(config, filePath);
			}
		}
		#endregion
	}
}
