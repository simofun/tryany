namespace Simofun.Unity.Data.Configuration.CustomAsset
{
    using Simofun.Unity.Data.Configuration.CustomAsset.Model;
    using Simofun.Unity.Data.Configuration.CustomAsset.Utils;
#if UNITY_EDITOR
    using UnityEditor;
    using UnityEditor.Callbacks;

    public class SimConfigCreateProcessor : AssetPostprocessor
    {
        #region Static Methods
        [RunAfterAssembly("Unity.Addressables.Editor")]
        static void OnPostprocessAllAssets(
            string[] importedAssets,
            string[] deletedAssets,
            string[] movedAssets,
            string[] movedFromAssetPaths,
            bool didDomainReload)
        {
            foreach (var str in importedAssets)
            {
                if (importedAssets == null || importedAssets.Length <= 0)
                {
                    continue;
                }

                var asset = SimCustomAssetConfigEditorUtils.LoadFromPath<SimCustomAssetConfigBase>(str);
                if (asset == null)
                {
                    continue;
                }

                if (SimCustomAssetConfigEditorUtils.IsDefaultsExist(asset))
                {
                    return;
                }

                SimConfigValidateProcessor.Validate(asset);
                SimCustomAssetConfigEditorUtils.CreateDefaults(asset);
            }
        }
        #endregion
    }
#endif
}
