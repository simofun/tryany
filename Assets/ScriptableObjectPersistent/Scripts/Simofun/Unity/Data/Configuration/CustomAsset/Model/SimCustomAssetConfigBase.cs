﻿//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigBase.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguç</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset.Model
{
	using Simofun.Unity.Data.Configuration.CustomAsset.Utils;
	using Sirenix.OdinInspector;
	using UnityEngine;

	public abstract class SimCustomAssetConfigBase : ScriptableObject, ISimCustomAssetConfig
	{
		#region Events
		public event System.Action<SimCustomAssetConfigBase> OnEditorValidation;
		#endregion

		#region Unity Methods
		#region Editor
		/// <inheritdoc />
		protected virtual void OnValidate() => this.OnEditorValidation?.Invoke(this);
		#endregion

		/// <inheritdoc />
		public virtual void Reset()
		{
#if UNITY_EDITOR
			this.LoadDefaults();
#endif
		}
		#endregion

		#region Public Methods
		[PropertyOrder(-1), ButtonGroup("Editor")]
		public virtual void LoadDefaults()
		{
#if UNITY_EDITOR
			SimCustomAssetConfigEditorUtils.LoadDefaults(this);
#endif
		}
		#endregion

		#region Methods
#if UNITY_EDITOR
		#region Editor
		[PropertyOrder(-1), ButtonGroup("Editor")]
		void UpdateDefaults()
		{
			SimCustomAssetConfigEditorUtils.UpdateDefaults(this);
		}
		#endregion
#endif
		#endregion
	}
}
