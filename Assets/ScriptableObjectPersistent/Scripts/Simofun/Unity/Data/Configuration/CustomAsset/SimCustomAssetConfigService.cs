//-----------------------------------------------------------------------
// <copyright file="SimCustomAssetConfigService.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tongu�</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.CustomAsset
{
	public class SimCustomAssetConfigService
		: SimCustomAssetConfigServiceBase<ISimCustomAssetConfigService, SimCustomAssetConfigService>
	{
	}
}
