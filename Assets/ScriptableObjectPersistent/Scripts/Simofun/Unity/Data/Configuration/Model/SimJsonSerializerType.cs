﻿//-----------------------------------------------------------------------
// <copyright file="SimJsonSerializerType.cs" company="Simofun">
//      Copyright (c) Simofun. All rights reserved.
// </copyright>
// <author>Samet Kurumahmut, Hasan Emre Tonguç</author>
//-----------------------------------------------------------------------

namespace Simofun.Unity.Data.Configuration.Model
{
	public enum SimJsonSerializerType
	{
		Unity,

		JsonNet
	}
}
